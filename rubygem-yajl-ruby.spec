%global gem_name yajl-ruby

# EPEL6 lacks rubygems-devel package that provides these macros
%if %{?el6}0
%global ruby_sitearch %(ruby -rrbconfig -e 'puts Config::CONFIG["sitearchdir"]')
%endif

Summary: Ruby C bindings to the excellent Yajl JSON stream-based parser library
Name: rubygem-%{gem_name}
Version: 1.2.0
Release: 2%{?dist}
Group: Development/Languages
License: MIT
URL: https://github.com/brianmario/yajl-ruby
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
%if %{?el6}0
Requires: ruby(abi) = 1.8
%else
Requires: ruby
%endif
Requires: ruby(rubygems)
BuildRequires: ruby-devel
BuildRequires: ruby(rubygems)
%{?el6:BuildRequires: ruby(abi) = 1.8}
%{!?el6:BuildRequires: rubygem(rspec)}
%{!?el6:BuildRequires: rubygems-devel}
Provides: rubygem(%{gem_name}) = %{version}

%description
Ruby C bindings to the excellent Yajl JSON stream-based parser library

%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
This package contains documentation for %{name}

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

# Remove developer-only files.
# https://github.com/brianmario/yajl-ruby/issues/103
for f in .gitignore .rspec .travis.yml Gemfile Rakefile; do
  rm $f
  sed -i "s|\"$f\",||g" %{gem_name}.gemspec
done
rm -r script
sed -i "s|\"script/[^\"]*\",||g" %{gem_name}.gemspec

%build
gem build %{gem_name}.gemspec

%if 0%{?el6}
mkdir -p .%{gem_dir}
export CONFIGURE_ARGS="--with-cflags='%{optflags}'"
gem install --local \
  --install-dir $(pwd)%{gem_dir} \
  -V \
  --force --rdoc \
  %{gem_name}-%{version}.gem
%else
%gem_install
%endif

# Remove unnecessary gemspec file
rm .%{gem_instdir}/%{gem_name}.gemspec

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

# Remove deprecated "ext" directory
rm -r %{buildroot}%{gem_instdir}/ext

# Move the binary extension.
# (NOTE: this is not compatible with EL6)
# Get the minor version number of Ruby
ruby_version_minor=$(ruby \
  -e "puts RUBY_VERSION.split('.')[1]")
# Move according to Ruby 2.1 or 2.0
if [ $ruby_version_minor -gt 0 ]; then
  # Ruby 2.1+ on Fedora 21 and above
  mkdir -p %{buildroot}%{gem_extdir_mri}
  cp -pa .%{gem_extdir_mri}/* %{buildroot}%{gem_extdir_mri}/
else
  # Ruby 2.0 on Fedora 20
  mkdir -p %{buildroot}%{gem_extdir_mri}/lib
  mv %{buildroot}%{gem_libdir}/yajl \
    %{buildroot}%{gem_extdir_mri}/lib
fi


# Fix permissions
# https://github.com/brianmario/yajl-ruby/issues/103
chmod -x %{buildroot}%{gem_instdir}/benchmark/subjects/unicode.json

%check
%if %{?el6}0
# spec on EL6 is too old; need RSpec2
%else
pushd .%{gem_instdir}
  rspec -I%{buildroot}%{gem_extdir_mri} spec
popd
%endif

%files
%doc %{gem_instdir}/MIT-LICENSE
%doc %{gem_instdir}/README.md
%dir %{gem_instdir}
%if 0%{?el6}
%{ruby_sitearch}/yajl/yajl.so
%else
%{gem_extdir_mri}
%endif
%{gem_libdir}
%{gem_spec}
%exclude %{gem_cache}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/CHANGELOG.md
%{gem_instdir}/benchmark
%{gem_instdir}/examples
%{gem_instdir}/spec
%{gem_instdir}/tasks

%changelog
* Fri Nov 07 2014 Ken Dreyer <ktdreyer@ktdreyer.com> - 1.2.0-2
- Update for F22 Ruby guidelines
- Drop EL6 compat when moving the binary yajl/yajl.so file in %%install

* Sat Feb 15 2014 Ken Dreyer <ktdreyer@ktdreyer.com> - 1.2.0-1
- Update to yajl-ruby 1.2.0

* Tue Dec 25 2012 Julian C. Dunn <jdunn@aquezada.com> - 1.1.0-2
- Unify EPEL and Fedora builds. Correct defects from review, bz#823351

* Mon Apr 30 2012  <rpms@courteau.org> - 1.1.0-1
- Initial package
